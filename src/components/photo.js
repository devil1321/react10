import React from 'react'
import {useGet} from 'restful-react'
function Photo({title,url}) {
    const {data:randomDog,loading, refetch} = useGet({
        path:url
    })
    console.log(randomDog)
    return loading ? <h1>Loading ...</h1> : (
        <div className="fetched__item">
            <div className='fetched__image'>
                <img alt='doggy' src={randomDog.message} />
            </div>
            <h2 className="fetched__title">{title}</h2>
            <button className="btn-fetch" onClick={()=>{refetch()}}>Losuj Psa</button>
        </div>
    )
}

export default Photo
