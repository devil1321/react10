import './App.css';
import Photo from './components/photo'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
function App() {
  return (
    <div className="App">
      <div className="fetched-wrapper">
      <Carousel
        
        autoPlay = {true}
        interval={3000}
        swipeable={true}
        emulateTouch={true}
        infiniteLoop={true}
        swipeScrollTolerance={100}
      >
        <Photo title = "Cairn" url='/cairn/images/random'/>
        <Photo title = "Pitbull" url='/pitbull/images/random'/>
        <Photo title = "Havanese" url='/havanese/images/random'/>
        <Photo title = "Boxer" url='/boxer/images/random'/>
        <Photo title = "Beagle" url='/beagle/images/random'/>
      </Carousel>
      </div>
  </div>
  );
}

export default App;
